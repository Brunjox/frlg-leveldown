# Pokémon FireRed and LeafGreen, Level Down version
This is a fork from a FRLG disassembly project found at https://github.com/pret/pokefirered.

It includes the following changes:
* any experience gain is negative, thus your Pokemon level down
* Rare Candies also level you down, but they retain their revival effect
* a Pokemon will never faint due to HP loss by leveling down (in battle or via Rare Candies); it will be at 1 HP instead
* Daycares do not make Pokemon gain nor lose any experience
* everything else is identical to the regular version of the games.

Changes with respect to the original code are marked by the following comment:
```
// LEVELDOWN
```
You can view all changes at once in commit [b3f91587](https://gitlab.com/Brunjox/frlg-leveldown/-/commit/b3f91587edbd3c2ae3b5417dbe09e4bb4fa84db0).

For further information, please refer to the README.md of the original project that was linked above.
